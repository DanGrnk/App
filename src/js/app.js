App = {
  web3Provider: null,
  contracts: {},
  account: 0x0,
  loading: false,

  init: function() {
    return App.initWeb3();
  },

  initWeb3: function() {
    // Initialize web3
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
    } else {
      // set the provider you want from Web3.providers
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
    }
    web3 = new Web3(App.web3Provider);

    App.displayAccountInfo();

    return App.initContract();
  },

  initContract: function() {
    $.getJSON('ChainList.json', function(chainListArtifact) {
      // Get the necessary contract artifact file and use it to instantiate a truffle contract abstraction.
      App.contracts.ChainList = TruffleContract(chainListArtifact);

      // Set the provider for our contract.
      App.contracts.ChainList.setProvider(App.web3Provider);

      App.listenToEvents();
      // Retrieve the article from the smart contract
      return App.reloadArticles();
    });
  },

  displayAccountInfo: function() {
    web3.eth.getCoinbase(function(err, account) {
      if (err === null) {
        App.account = account;
        $("#account").text(account);
        web3.eth.getBalance(account, function(err, balance) {
          if (err === null) {
            $("#accountBalance").text(web3.fromWei(balance, "ether") + " ETH");
          }
        });
      }
    });
  },

  sellArticle: function() {
    var _articleName = $("#articleName").val();
    var _articlePrice = web3.toWei($("#articlePrice").val(), "ether");
    var _articleDescription = $("#articleDescription").val();

    if(_articleName.trim() == "" || _articlePrice == 0) {
      return false;
    }

    App.contracts.ChainList.deployed().then(function(instance) {
      return instance.sellArticle(_articleName, _articleDescription, _articlePrice, { from: App.account, gas: 500000 });
    }).catch(function(error) {
      throw error;
    });
  },

  reloadArticles: function() {
    if(App.loading) {
      return;
    }
    App.loading = true;

    var contractInstance = null;

    App.contracts.ChainList.deployed().then(function(instance) {
      contractInstance = instance;
      return contractInstance.getArticlesForSale();
    }).then(function(salesArray) {
      let articlesRow = $("#articlesRow");

      if(salesArray.length == 0) {
        articlesRow.hide();
      } else {
        articlesRow.show();
        articlesRow.empty();
      }
      
      for(var i=0; i<salesArray.length; i++) {
        var articleID = salesArray[i].toNumber();
        contractInstance.articles(articleID).then(function(article) {
          App.displayArticle(article[0], article[1], article[3], article[4], article[5]);
        });
      }
      App.loading = false;
    }).catch(function(error) {
      App.loading = false;
      throw error;
    });
  },

  displayArticle: function(id, seller, name, description, price) {
    var articlesRow = $("#articlesRow");
    var articleTemplate = $("#articleTemplate");
    var priceEth = web3.fromWei(price, "ether");

    articleTemplate.find("#panel-title").text(name);
    articleTemplate.find("#article_id").text(id);
    articleTemplate.find("#article_price").text(priceEth + " ETH");
    articleTemplate.find("#article_description").text(description);
    articleTemplate.find(".btn-buy").attr("data-id", id)
    articleTemplate.find(".btn-buy").attr("data-value", priceEth)
    articleTemplate.find(".btn-buy").attr("id", id);
    if(seller == App.account) {
      articleTemplate.find("#article_seller").text("You");
      articleTemplate.find(".btn-buy").hide();
    } else {
      articleTemplate.find("#article_seller").text(seller);
      articleTemplate.find(".btn-buy").show();
    }

    articlesRow.append(articleTemplate.html());

    $("#" + id).click(function(event) {
  		App.buyArticle(event);
  	});
  },

  buyArticle: function(event) {
    event.preventDefault();

    var _priceEth = parseFloat($(event.target).data("value"));
    var _id = $(event.target).data('id');

    App.contracts.ChainList.deployed().then(function(instance) {
      return instance.buyArticle(_id, { from: App.account, value: web3.toWei(_priceEth, "ether"), gas: 500000});
    }).catch(function(error) {
      throw error;
    });
  },

  listenToEvents: function() {
    App.contracts.ChainList.deployed().then(function(instance) {
      instance.sellArticleEvent({}, { fromBlock: 0, toBlock: 'latest' }).watch(function(error, event) {
        if(!error) {
          $("#events").append('<li class="list-group-item list-group-item-info">FOR SALE: <strong>' + event.args._name + '</strong></li>');
        } else {
          console.log(error.message);
        }
        App.reloadArticles();
      });

      instance.buyArticleEvent({}, { fromBlock: 0, toBlock: 'latest' }).watch(function(error, event) {
        if(!error) {
          $("#events").append('<li class="list-group-item list-group-item-danger">ITEM BOUGHT: <strong>' + event.args._name + '</strong> - HAS BEEN BOUGHT BY: <strong>' + event.args._buyer + '</strong></li>');
        } else {
          console.log(error.message);
        }
        App.reloadArticles();
      });

    }).catch(function(error) {
      throw error;
    });
  },
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
